<?php

namespace Tsehelnyk\MyModule\Api;

interface CarManagementInterface
{
    /**
     * @param int $param
     * @return \Tsehelnyk\MyModule\Api\Data\CarInterface
     */
    public function getCar(int $param): \Tsehelnyk\MyModule\Api\Data\CarInterface;

    /**
     * @param \Tsehelnyk\MyModule\Api\Data\CarInterface
     * @return \Tsehelnyk\MyModule\Api\Data\CarInterface
     */
    public function createCar(\Tsehelnyk\MyModule\Api\Data\CarInterface $carInterface): \Tsehelnyk\MyModule\Api\Data\CarInterface;
}
