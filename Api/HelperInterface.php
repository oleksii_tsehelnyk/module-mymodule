<?php

namespace Tsehelnyk\MyModule\Api;

interface HelperInterface
{
    const STATIC_PHRASE_PATH = 'gen/gen_conf/show_static_phrases';
    const STATE_OF_MODULE_PATH = 'gen/gen_conf/show_status_of_MyModule_module';
    const IMAGE_PATH = 'gen/image_path/path_image';
    const ITEMS_PATH = 'frontend/items/path_image';

    /**
     * @param $path
     * @return string
     */
    public function getConfig($path): string;
}
