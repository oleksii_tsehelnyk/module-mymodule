<?php

namespace Tsehelnyk\MyModule\Api\Data;

interface InsuranceInterface
{
    const ID_INSURANCE = 'id_insurance';
    const NAME_INSURANCE = 'name_insurance';
    const CREATED_AT = 'created_at';

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @return mixed
     */
    public function getCreatedAt();

    /**
     * @param $name
     * @return InsuranceInterface
     */
    public function setName($name);

    /**
     * @param $createdAt
     * @return InsuranceInterface
     */
    public function setCreatedAt($createdAt);
}
