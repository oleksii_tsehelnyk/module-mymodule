<?php

namespace Tsehelnyk\MyModule\Api\Data;

interface CarInterface
{
    const ID_CAR = 'id_car';
    const MODEL = 'car_model';
    const YEAR = 'year';
    const PRICE = 'price';
    const CREATED_AT = 'created_at';
    const INSURANCE = 'id_insurance';

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @return string
     */
    public function getModel(): string;

    /**
     * @return int
     */
    public function getYear(): int;

    /**
     * @return float
     */
    public function getPrice(): float;

    /**
     * @return string
     */
    public function getCreatedAt(): string;

    /**
     * @return int
     */
    public function getInsurance(): int;

//    /**
//     * @return string
//     */
//    public function getNameInsurance(): string;

    /**
     * @param $model
     * @return CarInterface
     */
    public function setModel($model): CarInterface;

    /**
     * @param $year
     * @return CarInterface
     */
    public function setYear($year): CarInterface;

    /**
     * @param $price
     * @return CarInterface
     */
    public function setPrice($price): CarInterface;

    /**
     * @param $createdAt
     * @return CarInterface
     */
    public function setCreatedAt($createdAt): CarInterface;

    /**
     * @param $id
     * @return CarInterface
     */
    public function setInsurance($id): CarInterface;
}
