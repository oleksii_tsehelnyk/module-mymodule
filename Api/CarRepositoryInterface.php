<?php

namespace Tsehelnyk\MyModule\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
use Tsehelnyk\MyModule\Api\Data\CarInterface;

interface CarRepositoryInterface
{
    /**
     * @param CarInterface $car
     * @return CarInterface
     */
    public function save(CarInterface $car): CarInterface;

    /**
     * @return array
     */
    public function getListCustom(): array;

    /**
     * @param int $carId
     * @return CarInterface
     */
    public function getById(int $carId): CarInterface;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return SearchResults
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResults;

    /**
     * @param CarInterface $car
     * @return bool
     */
    public function delete(CarInterface $car): bool;

    /**
     * @param int $carId
     * @return bool
     */
    public function deleteById(int $carId): bool;
}
