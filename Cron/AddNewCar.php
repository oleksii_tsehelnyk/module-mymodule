<?php

namespace Tsehelnyk\MyModule\Cron;

use Exception;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Tsehelnyk\MyModule\Model\CarModelFactory;
use Tsehelnyk\MyModule\Model\CarRepository;

class AddNewCar
{
    /**
     * @var CarRepository
     */
    private CarRepository $carRepository;

    /**
     * @var CarModelFactory
     */
    private CarModelFactory $modelFactory;

    /**
     * @var DateTime
     */
    private DateTime $dateTime;

    /**
     * @param CarRepository $carRepository
     * @param CarModelFactory $modelFactory
     * @param DateTime $dateTime
     */
    public function __construct(
        CarRepository $carRepository,
        CarModelFactory $modelFactory,
        DateTime $dateTime
    )
    {
        $this->carRepository = $carRepository;
        $this->modelFactory = $modelFactory;
        $this->dateTime = $dateTime;
    }


    /**
     * @throws CouldNotSaveException
     * @throws Exception
     */
    public function execute()
    {
        $car = $this->modelFactory->create();

        $year = random_int(1950, 2021);
        $insurance = random_int(1, 2);
        $price = rand(2000, 150000);

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $car->setModel($randomString);
        $car->setYear($year);
        $car->setIdInsurance($insurance);
        $car->setPrice($price);
        $car->setCreatedAt($this->dateTime->gmtDate());

        $this->carRepository->save($car);
    }
}
