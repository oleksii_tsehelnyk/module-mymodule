<?php

namespace Tsehelnyk\MyModule\Setup\Patch\Data;

use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Tsehelnyk\MyModule\Model\CarModelFactory;
use Tsehelnyk\MyModule\Model\CarRepository;

class AddRowsToMyTable implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @var CarModelFactory
     */
    private $carModelFactory;

    /**
     * @var CarRepository
     */
    private $carRepository;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param DateTime $dateTime
     * @param CarModelFactory $carModelFactory
     * @param CarRepository $carRepository
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        DateTime $dateTime,
        CarModelFactory $carModelFactory,
        CarRepository $carRepository
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->dateTime = $dateTime;
        $this->carModelFactory = $carModelFactory;
        $this->carRepository = $carRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        echo 'Tsehelnyk_MyModule:AddNewRowToCarsTable:startSetup' . "\r\n";

        $newCar = $this->carModelFactory->create();

        $newCar->setModel('Toyota');
        $newCar->setYear(2012);
        $newCar->setIdInsurance(1);
        $newCar->setPrice(8500);
        $newCar->setCreatedAt($this->dateTime->gmtDate());

        $this->carRepository->save($newCar);

        $this->moduleDataSetup->endSetup();

        echo 'Tsehelnyk_MyModule:AddNewRowToCarsTable:endSetup' . "\r\n";
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

}
