<?php

namespace Tsehelnyk\MyModule\Plugin;

use Magento\Catalog\Model\Product;

class PluginForPrice
{
    /**
     * @param Product $product
     * @param $result
     * @return float
     */
    public function afterGetPrice(Product $product, $result): float
    {
        return $result = 33.3;
    }
}
