<?php

namespace Tsehelnyk\MyModule\Block;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Tsehelnyk\MyModule\Api\HelperInterface;
use Tsehelnyk\MyModule\Helper\ConfigData;
use Tsehelnyk\MyModule\Model\CarRepository;

class GetConfig extends Template
{
    /**
     * @var ConfigData
     */
    private ConfigData $helper;

    /**
     * @var CarRepository
     */
    private CarRepository $carRepository;

    /**
     * @param Template\Context $context
     * @param ConfigData $helper
     * @param CarRepository $carRepository
     * @param array $data
     */
    public function __construct(Template\Context $context, ConfigData $helper, CarRepository $carRepository, array $data = [])
    {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->carRepository = $carRepository;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->helper->getConfig(HelperInterface::IMAGE_PATH);
    }

    /**
     * @return string
     */
    public function getStateOfModule(): string
    {
        $index = $this->helper->getConfig(HelperInterface::STATE_OF_MODULE_PATH);
        if($index == 0) return 'no';
        else return 'yes';
    }

    /**
     * @return string
     */
    public function getPhrase(): string
    {
        $index = $this->helper->getConfig(HelperInterface::STATIC_PHRASE_PATH);
        if($index == 'item') return 'Lol';
        else if($index == 'item2') return 'Kek';
        else return 'Hello';
    }

    /**
     * @return array
     * @throws NoSuchEntityException
     */
    public function getItems(): array
    {
        $items = [];
        $indexes = explode(",",$this->helper->getConfig(HelperInterface::ITEMS_PATH));
        foreach ($indexes as $index)
        {
            $car = $this->carRepository->getById($index);
            array_push($items, $car);
        }
        return $items;
    }
}
