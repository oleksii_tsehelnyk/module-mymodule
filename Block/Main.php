<?php

namespace Tsehelnyk\MyModule\Block;

use Magento\Framework\View\Element\Template;

class Main extends Template
{

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getElement(): string
    {
        if($this->getData("ide")){
            return $this->getData("ide");
        }
        else return 999999;
    }
}
