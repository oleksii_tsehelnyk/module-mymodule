<?php
declare(strict_types=1);

namespace Tsehelnyk\MyModule\Block\Adminhtml\Buttons;

use Tsehelnyk\MyModule\Api\Data\CarInterface;
use Tsehelnyk\MyModule\Api\CarRepositoryInterface;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Logger\Handler\Exception;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var CarRepositoryInterface
     */
    private CarRepositoryInterface $carRepository;

    /**
     * @param Context $context
     * @param CarRepositoryInterface $carRepository
     */
    public function __construct(
        Context $context,
        CarRepositoryInterface $carRepository
    ) {
        $this->context = $context;
        $this->carRepository = $carRepository;
    }

    /**
     * @return int|null
     */
    public function getRowId(): ?int
    {
        try {
            $request = $this->context->getRequest();
            $rowID = (int)$request->getParam(CarInterface::ID_CAR);
            return (int)$this->carRepository->getById($rowID)->getId();
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param string $route
     * @param array $params
     * @return  string
     */
    public function getUrl(string $route = '', array $params = []): string
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
