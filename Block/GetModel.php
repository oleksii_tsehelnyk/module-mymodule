<?php

namespace Tsehelnyk\MyModule\Block;


use Magento\Framework\Api\Filter;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Tsehelnyk\MyModule\Api\Data\CarInterface;
use Tsehelnyk\MyModule\Model\CarRepository;

class GetModel extends Template
{
    /**
     * @var CarRepository
     */
    private CarRepository $carRepository;

    /**
     * @var FilterGroup
     */
    private FilterGroup $filterGroup;

    /**
     * @var Filter
     */
    private Filter $filter;

    /**
     * @var SortOrder
     */
    private SortOrder $sortOrder;

    /**
     * @var SearchCriteriaInterface
     */
    private SearchCriteriaInterface $searchCriteria;

    /**
     * @param Template\Context $context
     * @param CarRepository $carRepository
     * @param FilterGroup $filterGroup
     * @param Filter $filter
     * @param SortOrder $sortOrder
     * @param SearchCriteriaInterface $searchCriteria
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        CarRepository $carRepository,
        FilterGroup $filterGroup,
        Filter $filter,
        SortOrder $sortOrder,
        SearchCriteriaInterface $searchCriteria,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->carRepository = $carRepository;
        $this->filterGroup = $filterGroup;
        $this->filter = $filter;
        $this->sortOrder = $sortOrder;
        $this->searchCriteria = $searchCriteria;
    }

    /**
     * @return CarInterface|null
     * @throws NoSuchEntityException
     */
    public function getModel(): ?CarInterface
    {
        $id = $this->getData("id");
        if ($id)
        {
            return $this->carRepository->getById($id);
        }
        else {
            return null;
        }
    }

    /**
     * @return array
     */
    public function getCollection(): array
    {
        return $this->carRepository->getListCustom();
    }

    /**
     * @return SearchResults
     * @throws InputException
     */
    public function methodFroSearchCriteria(): SearchResults
    {
        $this->filter->setField("car_model")
            ->setValue("BMW")
            ->setConditionType("like");

        $this->sortOrder
            ->setField("price")
            ->setDirection("DESC");

        $this->filterGroup->setFilters([$this->filter]);

        $this->searchCriteria->setFilterGroups([$this->filterGroup]);

        $this->searchCriteria->setSortOrders([$this->sortOrder]);

        return $this->carRepository->getList($this->searchCriteria);

    }

}
