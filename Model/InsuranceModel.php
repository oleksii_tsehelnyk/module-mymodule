<?php

namespace Tsehelnyk\MyModule\Model;

use Magento\Framework\Model\AbstractModel;
use Tsehelnyk\MyModule\Api\Data\InsuranceInterface;

class InsuranceModel extends AbstractModel implements InsuranceInterface
{
    public function _construct() {
        $this->_init(InsuranceInterface::ID_INSURANCE);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(self::ID_INSURANCE);
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->getData(self::NAME_INSURANCE);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setName($name)
    {
        return $this->setData(self::NAME_INSURANCE, $name);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

}
