<?php

namespace Tsehelnyk\MyModule\Model;

use Magento\Catalog\Model\Category;

class ModelForProference extends Category
{

    /**
     * @return string
     */
    public function getName(): string
    {
        return "My Category";
    }
}
