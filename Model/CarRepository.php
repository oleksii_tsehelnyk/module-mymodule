<?php

namespace Tsehelnyk\MyModule\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Tsehelnyk\MyModule\Api\CarRepositoryInterface;
use Tsehelnyk\MyModule\Api\Data\CarInterface;
use Tsehelnyk\MyModule\Model\ResourceModel\Car;
use Tsehelnyk\MyModule\Model\ResourceModel\Car\CollectionFactory;

class CarRepository implements CarRepositoryInterface
{
    /**
     * @var CarModelFactory
     */
    private $carModelFactory;

    /**
     * @var Car
     */
    private $resource;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var SearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @param CarModelFactory $carModelFactory
     * @param Car $resource
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionFactory $collectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        CarModelFactory $carModelFactory,
        Car $resource,
        SearchResultsInterfaceFactory $searchResultsFactory,
        CollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor
    )
    {
        $this->carModelFactory = $carModelFactory;
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @inheritDoc
     * @throws CouldNotSaveException
     */
    public function save(CarInterface $car): CarInterface
    {
        try{
            /**
             * @var CarModel|CarInterface $car
             */
            $this->resource->save($car);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }

        return $car;
    }

    /**
     * @inheritDoc
     * @throws NoSuchEntityException
     */
    public function getById(int $carId): CarInterface
    {
        /**
         * @var CarModel|CarInterface $car
         */
        $car = $this->carModelFactory->create();
        $this->resource->load($car, $carId);
        if (!$car->getId()) {
            throw new NoSuchEntityException(__('Car with id `%1` does not exist.' . $carId, $carId));
        }

        return $car;
    }

    /**
     * @inheritdoc
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SearchResults
    {
        $colection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $colection);

        /** @var SearchResults $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($colection->getItems());
        $searchResults->setTotalCount($colection->getSize());

        return $searchResults;
    }


    /**
     * @inheritDoc
     */
    public function getListCustom(): array
    {
        $colection = $this->collectionFactory->create();
       // $col = $colection->getc(['car_id', 'car_model']);
        //$colection->addFilter('car_model', 'Audi')->getData();
        //$colection->addFieldToFilter('year', array('eq' => 2003));



        //$colection->getSelect()->order('price');

        //$colection->getSelect()->join('insurances', 'main_table.id_insurance = insurances.id_insurance', ['name_insurance'=>'name_insurance']);

        return $colection->getItems();
    }

    /**
     * @inheritDoc
     * @throws CouldNotDeleteException
     */
    public function delete(CarInterface $car): bool
    {
        try {
            /**
             * @var CarModel $car
             */
            $this->resource->delete($car);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }

        return true;
    }

    /**
     * @inheritDoc
     * @throws CouldNotDeleteException
     */
    public function deleteById(int $carId): bool
    {
        try {
            $delete = $this->delete($this->getById($carId));
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return $delete;
    }

}
