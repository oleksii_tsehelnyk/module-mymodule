<?php

namespace Tsehelnyk\MyModule\Model;

use Magento\Framework\Stdlib\DateTime\DateTime;
use Tsehelnyk\MyModule\Api\CarManagementInterface;
use Tsehelnyk\MyModule\Api\CarRepositoryInterface;

class CarManagement implements CarManagementInterface
{
    private CarRepositoryInterface $carRepository;
    private CarModelFactory $carModelFactory;
    private DateTime $dateTime;

    public function __construct(CarRepositoryInterface $carRepository, CarModelFactory $carModelFactory, DateTime $dateTime)
    {
        $this->carRepository = $carRepository;
        $this->carModelFactory = $carModelFactory;
        $this->dateTime = $dateTime;
    }

    /**
     * {@inheritdoc}
     */
    public function getCar(int $param): \Tsehelnyk\MyModule\Api\Data\CarInterface
    {
        return $this->carRepository->getById($param);
    }

    /**
     * {@inheritdoc}
     */
    public function createCar(\Tsehelnyk\MyModule\Api\Data\CarInterface $carModel): \Tsehelnyk\MyModule\Api\Data\CarInterface
    {
        return $this->carRepository->save($carModel);
    }


}
