<?php

namespace Tsehelnyk\MyModule\Model;

use Magento\Framework\Model\AbstractModel;
use Tsehelnyk\MyModule\Api\Data\CarInterface;
use Tsehelnyk\MyModule\Model\ResourceModel\Car;

class CarModel extends AbstractModel implements CarInterface
{

    public function _construct()
    {
        $this->_init(Car::class);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(self::ID_CAR);
    }

    /**
     * @inheritDoc
     */
    public function getModel(): string
    {
        return $this->getData(self::MODEL);
    }

    /**
     * @inheritDoc
     */
    public function getYear(): int
    {
        return $this->getData(self::YEAR);
    }

    /**
     * @inheritDoc
     */
    public function getPrice(): float
    {
        return $this->getData(self::PRICE);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): string
    {
        return $this->getData(self::CREATED_AT);
    }

//    /**
//     * @inheritDoc
//     */
//    public function getNameInsurance(): string
//    {
//       return $this->getData(InsuranceInterface::NAME_INSURANCE);
//    }

    /**
     * @inheritDoc
     */
    public function setModel($model): CarInterface
    {
        return $this->setData(self::MODEL, $model);
    }

    /**
     * @inheritDoc
     */
    public function setYear($year): CarInterface
    {
        return $this->setData(self::YEAR, $year);
    }

    /**
     * @inheritDoc
     */
    public function setPrice($price): CarInterface
    {
        return $this->setData(self::PRICE, $price);
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt($createdAt): CarInterface
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * @inheritDoc
     */
    public function getInsurance(): int
    {
        return $this->getData(self::INSURANCE);
    }

    /**
     * @inheritDoc
     */
    public function setInsurance($id): CarInterface
    {
        return $this->setData(self::INSURANCE, $id);
    }


}
