<?php

namespace Tsehelnyk\MyModule\Model\ResourceModel\Insurance;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Tsehelnyk\MyModule\Api\Data\InsuranceInterface;
use Tsehelnyk\MyModule\Model\InsuranceModel;
use Tsehelnyk\MyModule\Model\ResourceModel\Insurance;

class Collection extends AbstractCollection
{
    /**
     * {@inheritdoc }
     */
    protected $_idFieldName = InsuranceInterface::ID_INSURANCE;

    /**
     * {@inheritdoc}
     */
    public function _construct()
    {
        $this->_init(InsuranceModel::class, Insurance::class);
    }
}
