<?php

namespace Tsehelnyk\MyModule\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Tsehelnyk\MyModule\Api\Data\InsuranceInterface;

class Insurance extends AbstractDb
{
    const NAME_TABLE = 'insurances';

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(self::NAME_TABLE, InsuranceInterface::ID_INSURANCE);
    }

}
