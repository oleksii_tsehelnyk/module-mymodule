<?php

namespace Tsehelnyk\MyModule\Model\ResourceModel\Car;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Tsehelnyk\MyModule\Api\Data\CarInterface;
use Tsehelnyk\MyModule\Model\CarModel;
use Tsehelnyk\MyModule\Model\ResourceModel\Car;

class Collection extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected $_idFieldName = CarInterface::ID_CAR;

    /**
     * {@inheritdoc}
     */
    public function _construct()
    {
        $this->_init(CarModel::class, Car::class);
    }
}
