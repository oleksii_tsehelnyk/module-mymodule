<?php

namespace Tsehelnyk\MyModule\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Tsehelnyk\MyModule\Api\Data\CarInterface;

class Car extends AbstractDb
{
    const CAR_TABLE = 'cars_table';

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(self::CAR_TABLE, CarInterface::ID_CAR);
    }

}
