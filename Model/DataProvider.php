<?php
declare(strict_types=1);

namespace Tsehelnyk\MyModule\Model;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Tsehelnyk\MyModule\Api\Data\CarInterface;
use Tsehelnyk\MyModule\Model\ResourceModel\Car\CollectionFactory;
use Tsehelnyk\MyModule\Model\ResourceModel\Car\Collection;

/**
 * Class DataProvider
 * @package Tsehelnyk\MyModule\Model
 */
class DataProvider extends AbstractDataProvider
{
    /**
     * @var CollectionFactory
     */
    protected CollectionFactory $collectionFactory;

    /**
     * @var DataPersistorInterface
     */
    protected DataPersistorInterface $dataPersists;

    /**
     * @var array|null
     */
    protected $loadedData;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $weatherCollectionFactory
     * @param DataPersistorInterface $dataPersists
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $weatherCollectionFactory,
        DataPersistorInterface $dataPersists,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collectionFactory = $weatherCollectionFactory;
        $this->dataPersists = $dataPersists;
        $this->collection = $this->collectionFactory->create();
    }

    /**
     * {@inheritDoc}
     */
    public function getData()
    {
        /** @var Collection $collection */
        $this->collection = $this->collectionFactory->create();

        if ($this->loadedData === null) {
            $this->loadedData = [];
            $items = $this->collection->getItems();
            /** @var CarInterface $row */
            foreach ($items as $row) {
                $this->loadedData[$row->getId()] = $this->prepareData($row);
            }
            $data = $this->dataPersists->get('rows');
            if (!empty($data)) {
                $row = $this->collection->getNewEmptyItem();
                $row->setData($data);
                $this->loadedData[$row->getId()] = $this->prepareData($row);
                $this->dataPersists->clear('rows');
            }
        }
        return $this->loadedData;
    }

    /**
     * @param CarInterface $row
     * @return array
     */
    private function prepareData(CarInterface $row): array
    {
        return $row->getData();
    }
}
