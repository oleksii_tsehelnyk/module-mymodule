<?php

namespace Tsehelnyk\MyModule\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Tsehelnyk\MyModule\Api\CarRepositoryInterface;

class CarItems implements OptionSourceInterface
{
    /**
     * @var CarRepositoryInterface
     */
    private CarRepositoryInterface $carRepository;

    /**
     * @param CarRepositoryInterface $carRepository
     */
    public function __construct(CarRepositoryInterface $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        $array = [];
        $items = $this->carRepository->getListCustom();
        foreach ($items as $item)
        {
            array_push($array, ['value' => $item->getId(), 'label' => $item->getModel()]);
        }
        return $array;
    }
}
