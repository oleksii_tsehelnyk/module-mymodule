<?php

namespace Tsehelnyk\MyModule\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

class StaticPhrase implements OptionSourceInterface
{
    /**
     * @inheritDoc
     */
    public function toOptionArray()
    {
        return [
            'item' => 'Lol',
            'item2' => 'Kek',
            'item3' => 'Hello'
        ];
    }

}
