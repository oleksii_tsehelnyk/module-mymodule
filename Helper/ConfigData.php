<?php

namespace Tsehelnyk\MyModule\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Tsehelnyk\MyModule\Api\HelperInterface;

class ConfigData extends AbstractHelper implements HelperInterface
{
    /**
     * @inheritDoc
     */
    public function getConfig($path): string
    {
        return $this->scopeConfig->getValue($path);
    }
}
