<?php
declare(strict_types=1);

namespace Tsehelnyk\MyModule\Controller\Adminhtml\Index;

use Tsehelnyk\MyModule\Api\Data\CarInterface;
use Tsehelnyk\MyModule\Api\CarRepositoryInterface;
use Magento\Backend\App\Action as BackendAction;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class InlineEdit
 * @package Tsehelnyk\MyModule\Controller\Adminhtml\Index
 */
class InlineEdit extends BackendAction implements HttpPostActionInterface
{
    /**
     * {@inheritdoc}
     */
    const ADMIN_RESOURCE = 'Tsehelnyk_MyModule::car_inline_edit';

    /**
     * @var JsonFactory
     */
    private JsonFactory $jsonFactory;

    /**
     * @var CarRepositoryInterface
     */
    private CarRepositoryInterface $carRepository;

    /**
     * @param Context $context
     * @param CarRepositoryInterface $carRepository
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        CarRepositoryInterface $carRepository,
        JsonFactory $jsonFactory
    ) {
        $this->carRepository = $carRepository;
        $this->jsonFactory = $jsonFactory;
        parent::__construct($context);
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (empty($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $id) {
                    try {
                        /** @var CarInterface $model */
                        $model = $this->carRepository->getById((int)$id);
                        $model->setData(array_merge($model->getData(), $postItems[$id]));
                        $this->carRepository->save($model);
                    } catch (\Exception $e) {
                        $messages[] = $e->getMessage();
                        $error = true;
                    }
                }
            }
        }
        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }
}
