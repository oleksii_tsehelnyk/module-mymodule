<?php
declare(strict_types=1);

namespace Tsehelnyk\MyModule\Controller\Adminhtml\Index;

use Tsehelnyk\MyModule\Api\Data\CarInterface;
use Tsehelnyk\MyModule\Api\CarRepositoryInterface;
use Tsehelnyk\MyModule\Model\ResourceModel\Car\Collection as CarCollection;
use Tsehelnyk\MyModule\Model\ResourceModel\Car\CollectionFactory as CarResourceCollectionFactory;
use Exception;
use Magento\Backend\App\Action as BackendAction;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class MassDelete
 * @package Tsehelnyk\MyModule\Controller\Adminhtml\Index
 */
class MassDelete extends BackendAction implements HttpPostActionInterface
{
    /**
     * {@inheritdoc}
     */
    const ADMIN_RESOURCE = 'Tsehelnyk_MyModule::car_mass_delete';

    /**
     * @var DataPersistorInterface
     */
    private DataPersistorInterface $dataPersistor;

    /**
     * @var CarRepositoryInterface
     */
    private CarRepositoryInterface $carRepository;

    /**
     * @var Filter
     */
    private Filter $filter;

    /**
     * @var CarResourceCollectionFactory
     */
    private CarResourceCollectionFactory $collectionFactory;

    /**
     * @param Context $context
     * @param CarRepositoryInterface $carRepository
     * @param CarResourceCollectionFactory $collectionFactory
     * @param Filter $filter
     * @param DataPersistorInterface $dataPersist
     */
    public function __construct(
        Context $context,
        CarRepositoryInterface $carRepository,
        CarResourceCollectionFactory $collectionFactory,
        Filter $filter,
        DataPersistorInterface $dataPersist
    ) {
        $this->dataPersistor = $dataPersist;
        $this->filter = $filter;
        $this->carRepository = $carRepository;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        try {
            /** @var CarCollection $collection */
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $count = 0;
            foreach ($collection as $weather) {
                /** @var CarInterface $weather */
                if ($this->carRepository->delete($weather)) {
                    $count++;
                }
            }
            $message = __('A total of %1 record(s) have been deleted.', $count);
            $this->messageManager->addSuccessMessage($message);
            $this->dataPersistor->clear('row');
            return $resultRedirect->setPath('*/*/');
        } catch (NoSuchEntityException | LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while deleting rows.'));
        }
        return $resultRedirect->setPath('*/*/');
    }
}
