<?php
declare(strict_types=1);

namespace Tsehelnyk\MyModule\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action as BackendAction;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Tsehelnyk\MyModule\Api\Data\CarInterface;
use Tsehelnyk\MyModule\Api\CarRepositoryInterface;

/**
 * Class Delete
 * @package Tsehelnyk\MyModule\Controller\Adminhtml\Index
 */
class Delete extends BackendAction implements HttpGetActionInterface
{
    /**
     * {@inheritdoc}
     */
    const ADMIN_RESOURCE = 'Tsehelnyk_MyModule::car_delete';

    /**
     * @var CarRepositoryInterface
     */
    private CarRepositoryInterface $carRepository;

    /**
     * @var Context
     */
    private Context $context;

    /**
     * @param Context $context
     * @param CarRepositoryInterface $carRepository
     * @param DataPersistorInterface $dataPersist
     */
    public function __construct(
        Context $context,
        CarRepositoryInterface $carRepository,
        DataPersistorInterface $dataPersist
    ) {
        $this->dataPersist = $dataPersist;
        parent::__construct($context);
        $this->carRepository = $carRepository;
        $this->context = $context;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = (int)$this->getRequest()->getParam(CArInterface::ID_CAR);

        try {
            $this->carRepository->deleteById($id);
            $this->messageManager->addSuccessMessage(__('You deleted the row'));
            $this->dataPersist->clear('row');
            return $resultRedirect->setPath('*/*/');
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while deleting the row.'));
        }
        return $resultRedirect->setPath('*/*/');
    }
}
