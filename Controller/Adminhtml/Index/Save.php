<?php
declare(strict_types=1);

namespace Tsehelnyk\MyModule\Controller\Adminhtml\Index;

use Tsehelnyk\MyModule\Api\Data\CarInterface;
use Tsehelnyk\MyModule\Api\CarRepositoryInterface;
use Tsehelnyk\MyModule\Model\CarModelFactory;
use Magento\Backend\App\Action as BackendAction;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\Request\Http as HttpRequest;
use Magento\Framework\Controller\ResultInterface;

/**
 * Class Save
 * @package Tsehelnyk\MyModule\Controller\Adminhtml\Index
 */
class Save extends BackendAction implements HttpPostActionInterface
{
    /**
     * {@inheritdoc}
     */
    const ADMIN_RESOURCE = 'Tsehelnyk_car::car_save';

    /**
     * @var DataPersistorInterface
     */
    private DataPersistorInterface $dataPersist;

    /**
     * @var CarRepositoryInterface
     */
    private CarRepositoryInterface $carRepository;

    /**
     * @var CarInterface
     */
    private $carFactory;

    /**
     * @param Context $context
     * @param CarRepositoryInterface $carRepository
     * @param CarModelFactory $carFactory
     * @param DataPersistorInterface $dataPersist
     */
    public function __construct(
        Context $context,
        CarRepositoryInterface $carRepository,
        CarModelFactory $carFactory,
        DataPersistorInterface $dataPersist
    ) {
        $this->dataPersist = $dataPersist;
        $this->carRepository = $carRepository;
        $this->carFactory = $carFactory;
        parent::__construct($context);
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        /** @var HttpRequest $request */
        $request = $this->getRequest();
        $data = $request->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam(CarInterface::ID_CAR);
            if (empty($data[CarInterface::ID_CAR])) {
                $data[CarInterface::ID_CAR] = null;
            }

            if ($id) {
                /** @var CarInterface $model */
                $model = $this->carRepository->getById((int)$id);
            } else {
                /** @var CarInterface $model */
                $model = $this->carFactory->create();
            }
            $model->setData($data);

            try {
                $this->carRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the row.'));
                $this->dataPersist->clear('row');
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', [CarInterface::ID_CAR => $model->getId()]);
                }

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the row.'));
            }

            $this->dataPersist->set('vendor', $data);
            return $resultRedirect->setPath('*/*/edit', [CarInterface::ID_CAR => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
