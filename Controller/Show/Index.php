<?php

namespace Tsehelnyk\MyModule\Controller\Show;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\View\Result\PageFactory;
use Tsehelnyk\MyModule\Cron\AddNewCar;

class Index implements ActionInterface
{
    /**
     * @var PageFactory
     */
    private PageFactory $pageFactory;

    /**
     * @var Http
     */
    private Http $http;

    /**
     * @var AddNewCar
     */
    private AddNewCar $cron;

    /**
     * @param PageFactory $pageFactory
     * @param Http $http
     * @param AddNewCar $cron
     */
    public function __construct(
        PageFactory $pageFactory,
        Http $http,
        AddNewCar $cron
    )
    {
        $this->pageFactory = $pageFactory;
        $this->http = $http;
        $this->cron = $cron;
    }

    public function execute()
    {
        $page = $this->pageFactory->create();
        $block = $page->getLayout()->getBlock("show.model");
        $data = $this->http->getParam('id');
        if($data)
        {
            $block->setData("id", $data);
        }

        $this->cron->execute();

        return $page;
    }

}
