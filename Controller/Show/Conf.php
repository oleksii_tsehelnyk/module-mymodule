<?php

namespace Tsehelnyk\MyModule\Controller\Show;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\View\Result\PageFactory;

class Conf implements ActionInterface
{
    /**
     * @var PageFactory
     */
    private PageFactory $pageFactory;

    /**
     * @param PageFactory $pageFactory
     */
    public function __construct(PageFactory $pageFactory)
    {
        $this->pageFactory = $pageFactory;
    }

    public function execute()
    {
        $page = $this->pageFactory->create();
        $block = $page->getLayout()->getBlock('config.block');
        return $page;
    }
}
