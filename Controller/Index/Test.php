<?php

namespace Tsehelnyk\MyModule\Controller\Index;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\Manager;
use Magento\Framework\View\Result\PageFactory;

class Test implements ActionInterface
{
    /**
     * @var PageFactory
     */
    private PageFactory $pageFactory;

    /**
     * @var Http
     */
    private Http $http;

    /**$item->$
     * @var Manager
     */
    private Manager $manager;

    /**
     * @param PageFactory $pageFactory
     * @param Http $http
     * @param Manager $manager
     */
    public function __construct
    (
        PageFactory $pageFactory,
        Http $http,
        Manager $manager
    )
    {
        $this->pageFactory = $pageFactory;
        $this->http = $http;
        $this->manager = $manager;
    }

    public function execute()
    {
        $page = $this->pageFactory->create();
        $block = $page->getLayout()->getBlock("my.block");
        $data = $this->http->getParam('id');
        if($this->http->getParam('id'))
        {
            $block->setData("ide", $data);
        }

        $this->manager->dispatch('my_own_event', ['parameter' => $data]);

        return $page;
    }

}
